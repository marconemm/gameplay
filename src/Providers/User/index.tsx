import React, {
	createContext,
	useContext,
	useState,
	ReactNode,
	useEffect,
} from "react";
import type { AuthResponse, User } from "../../@Types";
import * as AuthSession from "expo-auth-session";
import api from "../../Services/api";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { USER_TAG } from "../../utils/db";

interface Props {
	user: User;
	userLoading: boolean;
	singIn: () => Promise<void>;
	logOut: () => void;
}

const UserContext = createContext({} as Props);

export const UserProvider = ({ children }: { children: ReactNode }) => {
	const [user, setUser] = useState<User>({} as User);
	const [isLoading, setIsLoading] = useState<boolean>(false);
	const { REDIRECT_URI } = process.env;
	const { SCOPE } = process.env;
	const { RESPONSE_TYPE } = process.env;
	const { CLIENT_ID } = process.env;
	const { CDN_IMAGE } = process.env;

	const singIn = async () => {
		try {
			setIsLoading(true);
			const { baseURL, headers } = api.defaults;

			const authUrl = `${baseURL}/oauth2/authorize?client_id=${CLIENT_ID}&redirect_uri=${REDIRECT_URI}&response_type=${RESPONSE_TYPE}&scope=${SCOPE}`;

			const { type, params } = (await AuthSession.startAsync({
				authUrl,
			})) as AuthResponse;

			if (type === "success" && !params.error) {
				headers.Authorization = `Bearer ${params.access_token}`;
				const response = await api.get("/users/@me");

				const { data } = response;

				data.avatar = `${CDN_IMAGE}/avatars/${data.id}/${data.avatar}.png`;
				data.token = params.access_token;
				if (data.username.includes(" ")) {
					data.firstName = data.username.split(" ")[0];
				} else {
					data.firstName = data.username;
				}

				// await AsyncStorage.clear();
				await AsyncStorage.setItem(USER_TAG, JSON.stringify(data));
				setUser({ ...data } as User);
			}
		} catch (error) {
			throw new Error("Ocorreu um erro durante a autenticação.");
		} finally {
			setIsLoading(false);
		}
	};

	const logOut = async () => {
		setUser({} as User);
		await AsyncStorage.removeItem(USER_TAG);
	};

	const getUserFromStorage = async () => {
		const storedData = await AsyncStorage.getItem(USER_TAG);

		if (storedData) {
			const localUser: User = JSON.parse(storedData);
			api.defaults.headers.Authorization = `Bearer ${localUser.token}`;

			setUser(localUser);
		}
	};

	useEffect(() => {
		getUserFromStorage();
	}, []);

	return (
		<UserContext.Provider
			value={{ user, userLoading: isLoading, singIn, logOut }}
		>
			{children}
		</UserContext.Provider>
	);
};

export const useUser = () => {
	//create the hook:
	const context = useContext(UserContext);

	if (!context) {
		throw new Error('"UserContext" must be inside of an "UserProvider"');
	}

	return context;
};
