import React from "react";
import type { ReactNode } from "react";
import { UserProvider } from "./User";

const Providers = ({ children }: { children: ReactNode }) => {
	return (
            <UserProvider>
                {children}
            </UserProvider>
    )
};

export default Providers;
