import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import Home from "../screens/Home";
import AppointmentDetails from "../screens/AppointmentDetails";
import AppointmentCreate from "../screens/AppointmentCreate";
import Theme from "../global/styles/theme";

const AuthRoutes = () => {
	const { colors } = Theme;
	const { Navigator, Screen } = createStackNavigator();

	return (
		<Navigator
			headerMode="none"
			screenOptions={{ cardStyle: { backgroundColor: colors.secondary800 } }}
		>
			<Screen name="Home" component={Home} />
			<Screen name="AppointmentDetails" component={AppointmentDetails} />
			<Screen name="AppointmentCreate" component={AppointmentCreate} />
		</Navigator>
	);
};

export default AuthRoutes;
