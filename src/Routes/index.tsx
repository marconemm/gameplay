import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import AuthRoutes from "./auth.routes";
import SingIn from "../screens/SingIn";
import { useUser } from "../Providers/User";

const Routes = () => {
	const { user } = useUser();

	return (
		<NavigationContainer>
			{user.token ? <AuthRoutes /> : <SingIn />}
		</NavigationContainer>
	);
};

export default Routes;
