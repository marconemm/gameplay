import { AuthSessionResult } from "expo-auth-session";
import { SvgProps } from "react-native-svg";

export interface User {
	id: string;
	username: string;
	avatar: string;
	firstName?: string;
	email: string;
	token: string;
}

export interface Category {
	id: string;
	title: string;
	icon: React.FC<SvgProps>;
}

export type AuthResponse = AuthSessionResult & {
	params: {
		access_token?: string;
		error: string;
	};
};

export interface Phrase {
	phrase: string;
	author: string;
}

export interface GuildProps {
	id: string;
	name: string;
	icon: React.FC<SvgProps> | null | string;
	owner: boolean;
}

export interface GuildIconProps {
	guildId: string;
	iconId: React.FC<SvgProps> | null | string;
}

export interface Appointment {
	id: string | number[];
	guild: GuildProps;
	category: string;
	date: string;
	description: string;
}

export interface AppointmentData {
	id: string;
	guild: GuildProps;
	category: string;
	date: string;
	description: string;
}

export interface RouteParams {
	item: AppointmentData;
}

export interface MemberData {
	id: string;
	username: string;
	avatar_url: string;
	status: string;
}

export interface WidgetData {
	id: string;
	name: string;
	instant_invite: string;
	members: MemberData[];
}
