import React from "react";
import { RectButton, RectButtonProps } from "react-native-gesture-handler";
import { LinearGradient } from "expo-linear-gradient";
import { SvgProps } from "react-native-svg";
import { View, Text } from "react-native";
import Styles from "./styles";
import Theme from "../../global/styles/theme";

interface PropsCategory extends RectButtonProps {
	title: string;
	icon: React.FC<SvgProps>;
	hasSelectedBox?: boolean;
	checked?: boolean;
}

const Category = ({
	title,
	icon: Icon,
	checked = false,
	hasSelectedBox = false,
	...rest
}: PropsCategory) => {
	const { secondary300, secondary500, secondary550, secondary200 } =
		Theme.colors;

	return (
		<RectButton {...rest}>
			<LinearGradient
				style={Styles.container}
				colors={[secondary300, secondary500]}
			>
				<LinearGradient
					style={[Styles.content, { opacity: checked ? 1 : 0.3 }]}
					colors={[checked ? secondary550 : secondary300, secondary200]}
				>
					{hasSelectedBox && (
						<View style={checked ? Styles.checked : Styles.unchecked} />
					)}

					<Icon width={48} height={48} />

					<Text style={Styles.title}>{title}</Text>
				</LinearGradient>
			</LinearGradient>
		</RectButton>
	);
};

export default Category;
