import { Inter_800ExtraBold } from "@expo-google-fonts/inter";
import { StyleSheet } from "react-native";
import Theme from "../../global/styles/theme";

const { colors, fonts } = Theme;

const Styles = StyleSheet.create({
	container: {
		width: 104,
		height: 120,
		justifyContent: "center",
		alignItems: "center",
		borderRadius: 8,
		marginRight: 8,
	},
	content: {
		width: 100,
		height: 116,
		borderRadius: 8,
		alignItems: "center",
		paddingVertical: 20,
		justifyContent: "space-between",
	},
	checked: {
		position: "absolute",
		top: 7,
		right: 7,
		width: 10,
		height: 10,
		backgroundColor: colors.primaryRed,

		borderRadius: 3,
	},
	unchecked: {
		position: "absolute",
		top: 7,
		right: 7,
		width: 12,
		height: 12,
		backgroundColor: colors.secondary800,

		borderColor: colors.secondary400,
		borderWidth: 2,
		borderRadius: 3,
	},
	title: {
		fontFamily: fonts.title700,
		color: colors.heading,
		fontSize: 15,
		marginTop: 15,
	},
});

export default Styles;
