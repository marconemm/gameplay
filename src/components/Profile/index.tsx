import React from "react";
import { View, Text, Alert } from "react-native";
import { useUser } from "../../Providers/User";
import Avatar from "../Avatar";
import Styles from "./styles";
import { RectButton } from "react-native-gesture-handler";

const Profile = () => {
	const { user, logOut } = useUser();

	const handleLogOut = () => {
		Alert.alert("Sair", "Deseja sair do aplicativo?", [
			{ text: "Não", style: "cancel" },
			{ text: "Sim", onPress: logOut },
		]);
	};
	return (
		<View style={Styles.container}>
			<RectButton onPress={handleLogOut}>
				<Avatar urlImage={user.avatar} />
			</RectButton>
			<View>
				<View style={Styles.user}>
					<Text style={Styles.greeting}>Olá,</Text>

					<Text style={Styles.username}>{user.username}!</Text>
				</View>
			</View>
		</View>
	);
};

export default Profile;
