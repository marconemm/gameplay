import { Inter_100Thin } from "@expo-google-fonts/inter";
import { StyleSheet } from "react-native";
import Theme from "../../global/styles/theme";

const { colors, fonts } = Theme;

const Styles = StyleSheet.create({
	container: {
		flexDirection: "row",
		alignItems: "center",
	},
	user: {
		flexDirection: "row",
	},
	greeting: {
		fontFamily: fonts.title500,
		fontSize: 24,
		color: colors.heading,
		marginRight: 6,
	},
	username: {
		fontFamily: fonts.title700,
		fontSize: 24,
		color: colors.heading,
	}
});

export default Styles;
