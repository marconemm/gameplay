import React from "react";
import { ScrollView } from "react-native";
import Styles from "./styles";
import { categories } from "../../utils/categories";
import Category from "../Category";

interface CategorySelectProps {
	categorySelected: string;
	setCategory: (categoryId: string) => void;
	hasSelectedBox?: boolean;
}

const CategorySelect = ({
	categorySelected,
	setCategory,
	hasSelectedBox = false,
}: CategorySelectProps) => {
	return (
		<ScrollView
			horizontal
			showsHorizontalScrollIndicator={false}
			contentContainerStyle={{ paddingRight: 40 }}
			style={Styles.container}
		>
			{categories.map(category => {
				return (
					<Category
						key={category.id}
						title={category.title}
						icon={category.icon}
						checked={category.id === categorySelected}
						hasSelectedBox={hasSelectedBox}
						onPress={() => setCategory(category.id)}
					/>
				);
			})}
		</ScrollView>
	);
};

export default CategorySelect;
