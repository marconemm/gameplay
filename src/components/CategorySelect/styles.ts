import { StyleSheet } from "react-native";
import Theme from "../../global/styles/theme";

const { colors, fonts } = Theme;

const Styles = StyleSheet.create({
	container: {
		minHeight: 120,
		maxHeight: 120,
		paddingLeft: 24,
		marginBottom: 24,
	},
});

export default Styles;
