import { StyleSheet } from "react-native";
import Theme from "../../global/styles/theme";

const { colors } = Theme;

const Styles = StyleSheet.create({
	wrapper: {
		width: "75%",
		height: 1,
		backgroundColor: colors.secondary200,
		// marginTop: 2,
		marginVertical: 10,
		alignSelf: "flex-end",
	},
});

export default Styles;
