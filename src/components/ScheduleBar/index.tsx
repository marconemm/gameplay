import React, { Dispatch, SetStateAction } from "react";
import { View, Text } from "react-native";
import Styles from "./styles";
import InputStyled from "../InputStyled";

interface Props {
	setDay: Dispatch<SetStateAction<string>>;
	setMonth: Dispatch<SetStateAction<string>>;
	setHour: Dispatch<SetStateAction<string>>;
	setMinute: Dispatch<SetStateAction<string>>;
}

const ScheduleBar = ({setDay, setMonth, setHour, setMinute}: Props) => {

	return (
		<View style={Styles.squareWrapper}>
			<View>
				<Text style={Styles.label}>Dia e Mês</Text>
				<View style={Styles.column}>
					<InputStyled
						maxLength={2}
						type="date"
						keyboardType={"numeric"}
						onChangeText={setDay}
					/>
					<Text style={Styles.separator}>/</Text>
					<InputStyled
						maxLength={2}
						type="date"
						keyboardType={"numeric"}
						onChangeText={setMonth}
					/>
				</View>
			</View>
			<View>
				<Text style={Styles.label}>Horário</Text>
				<View style={Styles.column}>
					<InputStyled
						maxLength={2}
						type="date"
						keyboardType={"numeric"}
						onChangeText={setHour}
					/>
					<Text style={Styles.separator}>:</Text>
					<InputStyled
						maxLength={2}
						type="date"
						keyboardType={"numeric"}
						onChangeText={setMinute}
					/>
				</View>
			</View>
		</View>
	);
};

export default ScheduleBar;
