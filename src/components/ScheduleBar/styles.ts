import { StyleSheet } from "react-native";
import Theme from "../../global/styles/theme";

const { colors, fonts } = Theme;

const Styles = StyleSheet.create({
	squareWrapper: {
		width: "100%",
		flexDirection: "row",
		justifyContent: "space-between",
		marginTop: 28,
	},
	label: {
		fontSize: 16,
		fontFamily: fonts.title700,
		color: colors.heading,
	},
	column: {
		flexDirection: "row",
		alignItems: "center",
	},
	separator: {
		marginRight: 4,
		fontSize: 15,
		fontFamily: fonts.text500,
		color: colors.highlight,
	},
});

export default Styles;
