import { StyleSheet } from "react-native";
import Theme from "../../../global/styles/theme";

const { colors, fonts } = Theme;

const Styles = StyleSheet.create({
	wrapper: {
		width: "100%",
		height: 56,
		backgroundColor: colors.primaryRed,
		borderRadius: 8,
		flexDirection: "row",
		alignItems: "center",
	},

	iconWrapper: {
		width: 56,
		height: 56,
		justifyContent: "center",
		alignItems: "center",
		borderRightWidth: 2,
		borderColor: colors.lineRed,
	},

	discordIcon: {
		width: 24,
		height: 18,
	},

	title: {
		flex: 1,
		color: colors.heading,
		textAlign: "center",
		fontSize: 15,
		fontFamily: fonts.text500,
	},
});

export default Styles;
