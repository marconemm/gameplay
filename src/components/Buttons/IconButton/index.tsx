import React from "react";
import { Text, View, Image } from "react-native";
import Styles from "./styles";
import discordImg from "../../../assets/discord.png";
import { RectButton, RectButtonProps } from "react-native-gesture-handler";

interface Props extends RectButtonProps {
	value: string;
}

const ButtonIcon = ({ value, ...rest }: Props) => {
	return (
		<RectButton style={Styles.wrapper} {...rest}>
			<View style={Styles.iconWrapper}>
				<Image style={Styles.discordIcon} source={discordImg}></Image>
			</View>
			<Text style={Styles.title}>{value}</Text>
		</RectButton>
	);
};

export default ButtonIcon;
