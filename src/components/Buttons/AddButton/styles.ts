import { StyleSheet } from "react-native";
import Theme from "../../../global/styles/theme";

const { primaryRed } = Theme.colors;

const Styles = StyleSheet.create({
	container: {
		height: 48,
		width: 48,
		alignItems: "center",
		justifyContent: "center",
		backgroundColor: primaryRed,
		borderRadius: 8,
	},
});

export default Styles;
