import React from "react";
import Styles from "./styles";
import Theme from "../../../global/styles/theme";
import { RectButton, RectButtonProps } from "react-native-gesture-handler";
import { Feather } from "@expo/vector-icons";

const AddButton = ({ ...rest }: RectButtonProps) => {
	const { heading } = Theme.colors;

	return (
		<RectButton style={Styles.container} {...rest}>
			<Feather name="plus-square" size={24} color={heading} />
		</RectButton>
	);
};

export default AddButton;
