import { StyleSheet } from "react-native";
import Theme from "../../../global/styles/theme";

const { colors, fonts } = Theme;

const Styles = StyleSheet.create({
	wrapper: {
		width: "100%",
		height: 56,
		backgroundColor: colors.primaryRed,
		borderRadius: 8,
		flexDirection: "row",
		alignItems: "center",
		alignSelf: "center",
		marginTop: 55,
	},

	title: {
		flex: 1,
		color: colors.heading,
		textAlign: "center",
		fontSize: 15,
		fontFamily: fonts.text500,
	},
});

export default Styles;
