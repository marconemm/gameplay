import React from "react";
import { Text } from "react-native";
import Styles from "./styles";
import { RectButton, RectButtonProps } from "react-native-gesture-handler";

interface Props extends RectButtonProps {
	value: string;
}

const RawButton = ({ value, ...rest }: Props) => {
	return (
		<RectButton style={Styles.wrapper} {...rest}>
			<Text style={Styles.title}>{value}</Text>
		</RectButton>
	);
};

export default RawButton;
