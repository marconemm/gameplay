import React from "react";
import { View, Text } from "react-native";
import GuildIcon from "../../components/GuildIcon";
import { RectButton, RectButtonProps } from "react-native-gesture-handler";
import Styles from "./styles";
import { categories } from "../../utils/categories";
import PlayerSvg from "../../assets/player.svg";
import CalendarSvg from "../../assets/calendar.svg";
import Theme from "../../global/styles/theme";
import type { AppointmentData } from "../../@Types";
import { LinearGradient } from "expo-linear-gradient";

interface AppointmentProps extends RectButtonProps {
	data: AppointmentData;
}

const Appointment = ({ data, ...rest }: AppointmentProps) => {
	const category = categories.find(item => item.id === data.category);
	const { primaryRed, on, secondary300, secondary500 } = Theme.colors;
	const {
		guild: { owner },
	} = data;

	return (
		<RectButton {...rest}>
			<View style={Styles.wrapper}>
				<LinearGradient
					style={Styles.iconWrapper}
					colors={[secondary300, secondary500]}
				>
					<GuildIcon guildId={data.guild.id} iconId={data.guild.icon} />
				</LinearGradient>

				<View style={Styles.content}>
					<View style={Styles.header}>
						<Text style={Styles.title}>{data.guild.name}</Text>
						<Text style={Styles.category}>{category?.title}</Text>
					</View>

					<View style={Styles.footer}>
						<View style={Styles.dateInfo}>
							<CalendarSvg />
							<Text style={Styles.date}>{data.date}</Text>
						</View>

						<View style={Styles.playerInfo}>
							<PlayerSvg fill={owner ? primaryRed : on} />
							<Text style={[Styles.player, { color: owner ? primaryRed : on }]}>
								{owner ? "Anfitrião" : "Visitante"}
							</Text>
						</View>
					</View>
				</View>
			</View>
		</RectButton>
	);
};

export default Appointment;
