import { StyleSheet } from "react-native";
import Theme from "../../global/styles/theme";

const { colors, fonts } = Theme;

const Styles = StyleSheet.create({
	wrapper: {
		width: "100%",
		flexDirection: "row",
		alignSelf: "center",
	},
	iconWrapper: {
		width: 68,
		height: 68,
		borderRadius: 8,
		alignItems: "center",
		justifyContent: "center",
		marginRight: 20,
	},
	content: {
		flex: 1,
	},

	header: {
		width: "100%",
		flexDirection: "row",
		justifyContent: "space-between",
		marginBottom: 12,
	},

	footer: {
		width: "100%",
		flexDirection: "row",
		justifyContent: "space-between",
	},
	dateInfo: {
		alignItems: "center",
		flexDirection: "row",
	},

	date: {
		fontFamily: fonts.text500,
		color: colors.heading,
		fontSize: 13,
		marginLeft: 7,
	},
	title: {
		fontFamily: fonts.title700,
		color: colors.heading,
		fontSize: 18,
	},

	category: {
		fontFamily: fonts.text400,
		color: colors.highlight,
		fontSize: 13,
		marginRight: 24,
	},

	playerInfo: {
		alignItems: "center",
		flexDirection: "row",
	},

	player: {
		fontFamily: fonts.text500,
		color: colors.highlight,
		fontSize: 13,
		marginLeft: 7,
		marginRight: 24,
	},
});

export default Styles;
