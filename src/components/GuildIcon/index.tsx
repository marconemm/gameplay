import React from "react";
import { Image, View } from "react-native";
import { GuildIconProps } from "../../@Types";
import Styles from "./styles";
import DiscordSvg from "../../assets/discord.svg";

const GuildIcon = ({ guildId, iconId }: GuildIconProps) => {
	const { CDN_IMAGE } = process.env;
	const uri = `${CDN_IMAGE}/icons/${guildId}/${iconId}`;

	return (
		<View style={Styles.wrapper}>
			{iconId ? (
				<Image source={{ uri }} style={Styles.image} resizeMode="cover" />
			) : (
				<DiscordSvg width={40} height={40} />
			)}
		</View>
	);
};

export default GuildIcon;
