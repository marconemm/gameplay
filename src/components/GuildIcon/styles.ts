import { StyleSheet } from "react-native";
import Theme from "../../global/styles/theme";

const { colors } = Theme;

const Styles = StyleSheet.create({
	wrapper: {
		width: 66,
		height: 66,
		borderRadius: 8,
		backgroundColor: colors.discord,
		alignItems: "center",
		justifyContent: "center",
	},
	image: {
		width: 64,
		height: 64,
		borderRadius: 8,
	},
});

export default Styles;
