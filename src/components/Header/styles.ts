import { StyleSheet } from "react-native";
import Theme from "../../global/styles/theme";
import { getStatusBarHeight } from "react-native-iphone-x-helper";

const { colors, fonts } = Theme;

const Styles = StyleSheet.create({
	wrapper: {
		width: "100%",
		height: 104,
		paddingTop: getStatusBarHeight(),
		paddingHorizontal: 24,
		flexDirection: "row",
		justifyContent: "center",
		alignItems: "center",
	},
	title: {
		flex: 1,
		textAlign: "center",
		fontFamily: fonts.title700,
		fontSize: 20,
		color: colors.heading,
	},
	emptyAction: {
		width: 25,
	},
});

export default Styles;
