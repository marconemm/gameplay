import { LinearGradient } from "expo-linear-gradient";
import React, { ReactNode } from "react";
import { BorderlessButton } from "react-native-gesture-handler";
import { Feather } from "@expo/vector-icons";
import { Text, View } from "react-native";
import Styles from "./styles";
import Theme from "../../global/styles/theme";
import { useNavigation } from "@react-navigation/native";

interface HeaderProps {
	title: string;
	action?: ReactNode;
}

const Header = ({ title, action }: HeaderProps) => {
	const navigation = useNavigation();
	const { secondary800, secondary200, heading } = Theme.colors;

	return (
		<LinearGradient
			style={Styles.wrapper}
			colors={[secondary800, secondary200]}
		>
			<BorderlessButton
				onPress={() => {
					navigation.goBack();
				}}
			>
				<Feather name="arrow-left" size={24} color={heading} />
			</BorderlessButton>

			<Text style={Styles.title}>{title}</Text>

			{action ? <View>{action}</View> : <View style={Styles.emptyAction} />}
		</LinearGradient>
	);
};

export default Header;
