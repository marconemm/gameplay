import { StyleSheet } from "react-native";

const Styles = StyleSheet.create({
	wrapper: {
		height: "80%",
		justifyContent: "center",
		alignContent: "center",
	},
});

export default Styles;
