import React from "react";
import { View } from "react-native";
import Styles from "./styles";
import Theme from "../../global/styles/theme";
import { ActivityIndicator } from "react-native";

const Loading: React.FC = () => {
	const { colors } = Theme;

	return (
		<View style={Styles.wrapper}>
			<ActivityIndicator size="large" color={colors.primaryRed} />
		</View>
	);
};

export default Loading;
