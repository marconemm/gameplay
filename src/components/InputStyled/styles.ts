import { StyleSheet } from "react-native";
import Theme from "../../global/styles/theme";

const { colors, fonts } = Theme;

const Styles = StyleSheet.create({
	date: {
		width: 48,
		height: 48,
		backgroundColor: colors.secondary200,
		color: colors.heading,
		borderRadius: 8,
		fontFamily: fonts.text400,
		fontSize: 14,
		marginRight: 4,
		textAlign: "center",
	},
	textArea: {
		textAlignVertical: "top",
		borderRadius: 8,
		color: colors.heading,
		backgroundColor: colors.secondary200,
		fontFamily: fonts.text400,
		fontSize: 14,
		paddingLeft: 15,
		paddingTop: 15,
	},
});

export default Styles;
