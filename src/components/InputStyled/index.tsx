import React from "react";
import { TextInput, TextInputProps } from "react-native";
import Styles from "./styles";

interface InputStyleProps extends TextInputProps {
	type: "date" | "textArea";
}

const InputStyled = ({
	type = "date",
	keyboardType,
	...rest
}: InputStyleProps) => {
	return (
		<TextInput style={Styles[type]} keyboardType={keyboardType} {...rest} />
	);
};

export default InputStyled;
