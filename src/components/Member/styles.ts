import { StyleSheet } from "react-native";
import Theme from "../../global/styles/theme";

const { colors, fonts } = Theme;

const Styles = StyleSheet.create({
	wrapper: {
		width: "100%",
		flexDirection: "row",
		alignItems: "center",
	},
	title: {
		fontFamily: fonts.title700,
		color: colors.heading,
		fontSize: 18,
	},
	status: {
		flexDirection: "row",
		alignItems: "center",
	},
	bulletStatus: {
		width: 8,
		height: 8,
		borderRadius: 4,
		marginRight: 10,
	},

	nameStatus: {
		fontFamily: fonts.title400,
		color: colors.highlight,
		fontSize: 14,
	},
});

export default Styles;
