import React from "react";
import { View, Text } from "react-native";
import Styles from "./styles";
import Avatar from "../Avatar";
import Theme from "../../global/styles/theme";
import { MemberData } from "../../@Types";

interface MemberProps {
	data: MemberData;
}

const Member = ({ data }: MemberProps) => {
	const { on, primaryRed, primaryYellow } = Theme.colors;

	return (
		<View style={Styles.wrapper}>
			<Avatar urlImage={data.avatar_url}></Avatar>

			<View>
				<Text style={Styles.title}>{data.username}</Text>

				<View style={Styles.status}>
					<View
						style={[
							Styles.bulletStatus,
							{
								backgroundColor:
									data.status === "online"
										? on
										: data.status === "dnd"
										? primaryYellow
										: primaryRed,
							},
						]}
					/>

					<Text style={Styles.nameStatus}>
						{data.status === "online"
							? "Disponível"
							: data.status === "dnd"
							? "Ausente"
							: "Ocupado"}
					</Text>
				</View>
			</View>
		</View>
	);
};

export default Member;
