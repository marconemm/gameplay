import React from "react";
import { Text } from "react-native";
import { View } from "react-native";
import Styles from "./styles";

interface ListHeaderProps {
	title: string;
	subtitle: string;
}

const ListHeader = ({ title, subtitle }: ListHeaderProps) => {
	return (
		<View style={Styles.container}>
			<Text style={Styles.title}>{title}</Text>

			<Text style={Styles.subtitle}>{subtitle}</Text>
		</View>
	);
};

export default ListHeader;
