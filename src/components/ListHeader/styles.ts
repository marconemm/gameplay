import { StyleSheet } from "react-native";
import Theme from "../../global/styles/theme";

const { colors, fonts } = Theme;

const Styles = StyleSheet.create({
	container: {
		width: "100%",
		flexDirection: "row",
		justifyContent: "space-between",
		paddingHorizontal: 24,
	},
	title: {
		fontFamily: fonts.title700,
		color: colors.heading,
		fontSize: 18,
	},
	subtitle: {
		fontFamily: fonts.title400,
		color: colors.highlight,
		fontSize: 13,
	},
});

export default Styles;
