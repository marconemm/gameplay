import React, { ReactNode } from "react";
import {
	View,
	Modal,
	ModalProps,
	TouchableWithoutFeedback,
} from "react-native";
import Background from "../Background";
import Styles from "./styles";

interface Props extends ModalProps {
	children: ReactNode;
	closeModal: () => void;
}

const ModalView = ({ children, closeModal, ...rest }: Props) => {
	return (
		<Modal transparent animationType={"slide"} statusBarTranslucent {...rest}>
			<TouchableWithoutFeedback onPress={closeModal}>
				<View style={Styles.overlay}>
					<View style={Styles.wrapper}>
						<Background>
							<View style={Styles.bar} />
							{children}
						</Background>
					</View>
				</View>
			</TouchableWithoutFeedback>
		</Modal>
	);
};

export default ModalView;
