import { StyleSheet } from "react-native";
import Theme from "../../global/styles/theme";

const { colors, fonts } = Theme;

const Styles = StyleSheet.create({
	overlay: {
		flex: 1,
		backgroundColor: colors.overlay,
	},
	wrapper: {
		flex: 1,
		marginTop: 100,
	},
	bar: {
		width: 39,
		height: 2,
		borderRadius: 2,
		backgroundColor: colors.secondary100,
		alignSelf: "center",
		marginTop: 13,
		marginBottom: 50,
	},
});

export default Styles;
