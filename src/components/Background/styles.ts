import { StyleSheet } from "react-native";
import Theme from "../../global/styles/theme";

const { colors } = Theme;

const Styles = StyleSheet.create({
	container: { flex: 1 },
});

export default Styles;
