import React, { ReactNode } from "react";
import { LinearGradient } from "expo-linear-gradient";
import Theme from "../../global/styles/theme";
import Styles from "./styles";

interface Props {
	children: ReactNode;
}

const Background = ({ children }: Props) => {
	const { secondary800, secondary600 } = Theme.colors;

	return (
		<LinearGradient
			style={Styles.container}
			colors={[secondary600, secondary800]}
		>
			{children}
		</LinearGradient>
	);
};

export default Background;
