import React from "react";
import { LinearGradient } from "expo-linear-gradient";
import Styles from "./styles";
import Theme from "../../global/styles/theme";
import { Image } from "react-native";

interface Props {
	urlImage: string;
}

const Avatar = ({ urlImage }: Props) => {
	const { secondary300, secondary500 } = Theme.colors;

	return (
		<LinearGradient
			style={Styles.container}
			colors={[secondary300, secondary500]}
		>
			<Image source={{ uri: urlImage }} style={Styles.avatar}></Image>
		</LinearGradient>
	);
};

export default  Avatar;
