import React from "react";
import {
	View,
	Text,
	TouchableOpacity,
	TouchableOpacityProps,
} from "react-native";
import GuildIcon from "../../components/GuildIcon";
import Styles from "./styles";
import { Feather } from "@expo/vector-icons";
import Theme from "../../global/styles/theme";
import { GuildProps } from "../../@Types";

interface Props extends TouchableOpacityProps {
	data: GuildProps;
}

const Guild = ({ data, ...rest }: Props) => {
	const { heading } = Theme.colors;

	return (
		<TouchableOpacity style={Styles.wrapper} activeOpacity={0.7} {...rest}>
			<GuildIcon guildId={data.id} iconId={data.icon} />

			<View style={Styles.content}>
				<View>
					<Text style={Styles.title}>{data.name}</Text>
					<Text style={Styles.role}>
						{data.owner ? "Administrador" : "Convidado"}
					</Text>
				</View>
			</View>
			<Feather name="chevron-right" color={heading} size={24} />
		</TouchableOpacity>
	);
};

export default Guild;
