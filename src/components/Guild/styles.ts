import { StyleSheet } from "react-native";
import Theme from "../../global/styles/theme";

const { colors, fonts } = Theme;

const Styles = StyleSheet.create({
	wrapper: {
		width: "100%",
		flexDirection: "row",
		alignItems: "center",
		paddingHorizontal: 24,
	},
	content: {
		flex: 1,
		justifyContent: "center",
		marginLeft: 20,
	},
	title: {
		fontFamily: fonts.title700,
		color: colors.heading,
		fontSize: 18,
		marginBottom: 11,
	},
	role: {
		fontFamily: fonts.title500,
		color: colors.highlight,
		fontSize: 14,
	},
});

export default Styles;
