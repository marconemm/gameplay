import React, { useEffect, useState } from "react";
import { View, FlatList } from "react-native";
import type { GuildProps } from "../../components/Guild";
import Guild from "../../components/Guild";
import Loading from "../../components/Loading";
import HorizontalRule from "../../components/HorizontalRule";
import Styles from "./styles";
import api from "../../Services/api";

interface Props {
	handleSelectGuild: (guild: GuildProps) => void;
}

const GuildsModal = ({ handleSelectGuild }: Props) => {
	const [guildsList, setGuildsList] = useState<GuildProps[]>([]);
	const [isLoading, setIsLoading] = useState<boolean>(true);

	useEffect(() => {
		api.get("/users/@me/guilds").then(response => {
			setGuildsList(response.data);
			setIsLoading(false);
		});
	}, []);

	return (
		<View style={Styles.wrapper}>
			{isLoading ? (
				<Loading />
			) : (
				<FlatList
					data={guildsList}
					keyExtractor={item => item.id}
					renderItem={({ item }) => (
						<Guild data={item} onPress={() => handleSelectGuild(item)} />
					)}
					ListHeaderComponent={() => <HorizontalRule />}
					ItemSeparatorComponent={() => <HorizontalRule />}
					showsVerticalScrollIndicator={false}
					contentContainerStyle={{ paddingBottom: 85 }}
					style={Styles.guildsList}
				/>
			)}
		</View>
	);
};

export default GuildsModal;
