import { StyleSheet } from "react-native";
import Theme from "../../global/styles/theme";

const { colors, fonts } = Theme;

const Styles = StyleSheet.create({
	wrapper: {
		alignItems: "center",
		paddingTop: 24,
	},
	guildsList: {
		width: "100%",
	},
});

export default Styles;
