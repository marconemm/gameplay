import React, { useCallback, useEffect, useMemo, useState } from "react";
import { View } from "react-native";
import Styles from "./styles";
import Profile from "../../components/Profile";
import AddButton from "../../components/Buttons/AddButton";
import HorizontalRule from "../../components/HorizontalRule";
import Background from "../../components/Background";
import Appointment from "../../components/Appointment";
import CategorySelect from "../../components/CategorySelect";
import ListHeader from "../../components/ListHeader";
import { FlatList } from "react-native";
import { useFocusEffect, useNavigation } from "@react-navigation/native";
import { Text } from "react-native";
import { AppointmentData, Phrase } from "../../@Types";
import { useUser } from "../../Providers/User";
import { APPOINTMENTS_TAG } from "../../utils/db";
import AsyncStorage from "@react-native-async-storage/async-storage";
import Loading from "../../components/Loading";

const Home = () => {
	const [category, setCategory] = useState<string>("");
	const [selectedPhrase, setSelectedPhrase] = useState<Phrase>({} as Phrase);
	const navigation = useNavigation();
	const { userLoading } = useUser();
	const [appointmentsList, setAppointments] = useState<AppointmentData[]>([]);
	const [isLoading, setIsLoading] = useState<boolean>(true);

	const phraseList: Phrase[] = useMemo(
		() => [
			{
				phrase: "Desejar algo, não lhe dá o direito de tê-lo.",
				author: "Assassin's Creed II",
			},
			{
				phrase:
					"É fácil esquecer o que é um pecado no meio de um campo de batalha.",
				author: "Metal Gear Solid",
			},
			{ phrase: "Apague minhas chamas com gasolina.", author: "Max Payne" },
			{
				phrase: "A verdadeira vitória é dar tudo de si, sem arrependimentos.",
				author: "Street Fighter Alpha 3",
			},
			{
				phrase:
					"A esperança é o que nos fortalece. É por isso que estamos aqui.",
				author: "God of War III",
			},
			{ phrase: "Finish him!", author: "Mortal Kombat" },
			{ phrase: "Guerra nunca muda.", author: "Fallout: New Vegas" },
			{
				phrase: "Mesmo quando a vida é tão injusta, não desista.",
				author: "Assassin’s Creed II",
			},
			{
				phrase:
					"Muitas vezes, quando adivinhamos os motivos dos outros, revelamos apenas os nossos.",
				author: "Destiny",
			},
			{
				phrase: "Nada é mais durão do que tratar uma mulher com respeito.",
				author: "Borderlands 2",
			},
			{
				phrase: "Não há heróis aqui, apenas sobreviventes!",
				author: "Tomb Raider",
			},
			{
				phrase: "Não importa pelo quê, você tem que encontrar algo para lutar.",
				author: "The Last of Us",
			},
			{
				phrase: "Ninguém é perfeito, até que você se apaixone por ele.",
				author: "Final Fantasy VII",
			},
			{
				phrase: "O extraordinário está no que fazemos, não no que somos.",
				author: "Tomb Raider",
			},
		],
		[userLoading],
	);

	const getAppointments = async () => {
		const storage = await AsyncStorage.getItem(APPOINTMENTS_TAG);
		const storedAppointments: AppointmentData[] = storage
			? JSON.parse(storage)
			: [];

		if (category) {
			setAppointments(
				storedAppointments.filter(item => item.category === category),
			);
		} else {
			setAppointments(storedAppointments);
		}
		setIsLoading(false);
	};

	useFocusEffect(
		useCallback(() => {
			getAppointments();
		}, [category]),
	);

	const handleCategorySelect = (categoryId: string) => {
		categoryId === category ? setCategory("") : setCategory(categoryId);
	};

	useEffect(() => {
		const randIndex = Math.floor(Math.random() * phraseList.length);
		setSelectedPhrase(phraseList[randIndex]);
	}, []);

	return (
		<Background>
			<View style={Styles.header}>
				<Profile />
				<AddButton onPress={() => navigation.navigate("AppointmentCreate")} />
			</View>

			<View style={Styles.messageWrapper}>
				<Text style={Styles.message}>"{selectedPhrase.phrase}"</Text>
				<Text style={Styles.author}>({selectedPhrase.author})</Text>
			</View>
			<View>
				<CategorySelect
					categorySelected={category}
					setCategory={handleCategorySelect}
				/>
			</View>
			{isLoading ? (
				<Loading />
			) : (
				<>
					<ListHeader
						title={"Partidas agendadas"}
						subtitle={`total ${appointmentsList.length}`}
					/>

					<FlatList
						data={appointmentsList}
						keyExtractor={item => item.id}
						renderItem={({ item }) => (
							<Appointment
								data={item}
								onPress={() => {
									navigation.navigate("AppointmentDetails", { item });
								}}
							/>
						)}
						style={Styles.matches}
						showsVerticalScrollIndicator={false}
						contentContainerStyle={{ paddingBottom: 25 }}
						ItemSeparatorComponent={() => <HorizontalRule />}
					/>
				</>
			)}
		</Background>
	);
};

export default Home;
