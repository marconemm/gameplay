import { StyleSheet } from "react-native";
import Theme from "../../global/styles/theme";
import { getStatusBarHeight } from "react-native-iphone-x-helper";

const { colors, fonts } = Theme;

const Styles = StyleSheet.create({
	header: {
		width: "100%",
		paddingHorizontal: 24,
		flexDirection: "row",
		justifyContent: "space-between",
		marginTop: getStatusBarHeight() + 26,
		marginBottom: 10,
	},
	messageWrapper: {
		maxWidth: "100%",
		marginBottom: 30,
	},
	message: {
		fontFamily: fonts.text500,
		color: colors.heading,
		textAlign: "center",
	},
	author: {
		fontFamily: fonts.text400,
		color: colors.highlight,
		fontSize: 10,
		textAlign: "center",
	},
	matches: {
		marginTop: 25,
		marginLeft: 25,
	},
});

export default Styles;
