import { StyleSheet } from "react-native";
import Theme from "../../global/styles/theme";

const { colors, fonts } = Theme;

const Styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: "center",
		justifyContent: "center",
	},

	image: {
		width: "100%",
		height: 360,
	},

	content: {
		marginTop: -60,
		paddingHorizontal: 50,
	},

	title: {
		textAlign: "center",
		fontSize: 40,
		marginBottom: 16,
		color: colors.heading,
		fontFamily: fonts.title700,
		lineHeight: 40,
	},

	subtitle: {
		textAlign: "center",
		fontSize: 15,
		marginBottom: 64,
		color: colors.heading,
		fontFamily: fonts.title500,
		lineHeight: 25,
	},
});

export default Styles;
