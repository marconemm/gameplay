import React from "react";
import { Text, View, Image, Alert, ActivityIndicator } from "react-native";
import Styles from "./styles";
import illustrationImg from "../../assets/illustration.png";
import ButtonIcon from "../../components/Buttons/IconButton";
import Background from "../../components/Background";
import { useUser } from "../../Providers/User";
import Theme from "../../global/styles/theme";

const SingIn = () => {
	const { userLoading, singIn } = useUser();

	const handleSingIn = async () => {
		try {
			await singIn();
		} catch (error) {
			Alert.alert(error.message);
		}
	};

	return (
		<Background>
			<View style={Styles.container}>
				<Image
					style={Styles.image}
					source={illustrationImg}
					resizeMode={"stretch"}
				/>
				<View style={Styles.content}>
					<Text style={Styles.title}>Conecte-se e organize suas jogatinas</Text>
					<Text style={Styles.subtitle}>
						Crie grupos para jogar seus games favoritos com seus amigos
					</Text>
					{userLoading ? (
						<ActivityIndicator color={Theme.colors.primaryRed} />
					) : (
						<ButtonIcon value={"Entrar com Discord"} onPress={handleSingIn} />
					)}
				</View>
			</View>
		</Background>
	);
};

export default SingIn;
