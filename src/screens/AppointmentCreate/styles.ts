import { StyleSheet } from "react-native";
import { getBottomSpace } from "react-native-iphone-x-helper";
import Theme from "../../global/styles/theme";

const { colors, fonts } = Theme;

const Styles = StyleSheet.create({
	wrapper: {
		flex: 1,
	},
	label: {
		fontSize: 16,
		fontFamily: fonts.title700,
		color: colors.heading,
	},
	form: {
		paddingHorizontal: 24,
	},

	select: {
		width: "100%",
		height: 68,
		flexDirection: "row",
		alignItems: "center",
		borderColor: colors.secondary300,
		borderWidth: 1,
		borderRadius: 8,
		paddingRight: 25,
	},
	image: {
		width: 64,
		height: 68,
		backgroundColor: colors.secondary200,
		borderColor: colors.secondary300,
		borderWidth: 1,
		borderRadius: 8,
	},
	selectContent: {
		flex: 1,
		alignItems: "center",
	},
	labelsWrapper: {
		width: "100%",
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "space-between",
		marginTop: 28,
		marginBottom: 12,
	},
	maxInputText: {
		fontSize: 14,
		fontFamily: fonts.title400,
		color: colors.highlight,
	},
	buttonWrapper: {
		paddingHorizontal: 24,
		marginBottom: getBottomSpace()+40,
	},
	modalWrapper: {
		flex: 1,
		backgroundColor: colors.overlay,
		marginTop: 100,
	},
	modalContent: {
		width: "100%",
	},
	modalBar: {
		width: 39,
		height: 2,
		borderRadius: 2,
		backgroundColor: colors.secondary100,
		alignSelf: "center",
		marginTop: 14,
	},
});

export default Styles;
