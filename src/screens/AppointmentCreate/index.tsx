import React, { useState } from "react";
import {
	View,
	Text,
	KeyboardAvoidingView,
	ScrollView,
	Platform,
} from "react-native";
import Styles from "./styles";
import CategorySelect from "../../components/CategorySelect";
import Background from "../../components/Background";
import Header from "../../components/Header";
import GuildIcon from "../../components/GuildIcon";
import GuildsModal from "../GuildsModal";
import ModalView from "../../components/ModalView";
import InputStyled from "../../components/InputStyled";
import ScheduleBar from "../../components/ScheduleBar";
import { RectButton } from "react-native-gesture-handler";
import { Feather } from "@expo/vector-icons";
import Theme from "../../global/styles/theme";
import RawButton from "../../components/Buttons/RawButton";
import uuid from "react-native-uuid";
import type { Appointment, GuildProps } from "../../@Types";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { APPOINTMENTS_TAG } from "../../utils/db";
import { useNavigation } from "@react-navigation/native";

const AppointmentCreate: React.FC = () => {
	const [category, setCategory] = useState<string>("");
	const [openGuildModal, setOpenGuildModal] = useState<boolean>(false);
	const [currentGuild, setCurrentGuild] = useState<GuildProps>(
		{} as GuildProps,
	);

	const [day, setDay] = useState<string>("");
	const [month, setMonth] = useState<string>("");
	const [hour, setHour] = useState<string>("");
	const [minute, setMinute] = useState<string>("");
	const [description, setDescription] = useState<string>("");
	const { heading } = Theme.colors;
	const navigation = useNavigation();

	const handleSelectGuild = (selectedGuild: GuildProps) => {
		setCurrentGuild(selectedGuild);
		setOpenGuildModal(false);
	};

	const handleSave = async () => {
		const newAppointment: Appointment = {
			id: uuid.v4(),
			guild: currentGuild,
			category,
			date: `${day}/${month} às ${hour}:${minute}h`,
			description,
		};

		const storage = await AsyncStorage.getItem(APPOINTMENTS_TAG);
		const appointments = storage ? JSON.parse(storage) : [];

		await AsyncStorage.setItem(
			APPOINTMENTS_TAG,
			JSON.stringify([...appointments, newAppointment]),
		);

		navigation.navigate("Home");
	};

	return (
		<KeyboardAvoidingView
			behavior={Platform.OS === "ios" ? "padding" : "height"}
			style={Styles.wrapper}
		>
			<ScrollView>
				<Background>
					<Header title={"Agendar partida"} />

					<Text
						style={[
							Styles.label,
							{ marginTop: 18, marginLeft: 24, marginBottom: 18 },
						]}
					>
						Categoria
					</Text>

					<CategorySelect
						hasSelectedBox
						categorySelected={category}
						setCategory={setCategory}
					/>
					<View style={Styles.form}>
						<RectButton onPress={() => setOpenGuildModal(true)}>
							<View style={Styles.select}>
								{currentGuild.icon ? (
									<GuildIcon
										guildId={currentGuild.id}
										iconId={currentGuild.icon}
									/>
								) : (
									<View style={Styles.image} />
								)}

								<View style={Styles.selectContent}>
									<Text style={Styles.label}>
										{currentGuild.name
											? currentGuild.name
											: "Selecione um servidor"}
									</Text>
								</View>

								<Feather name="chevron-right" color={heading} size={18} />
							</View>
						</RectButton>
						<ScheduleBar
							setDay={setDay}
							setHour={setHour}
							setMonth={setMonth}
							setMinute={setMinute}
						/>

						<View style={Styles.labelsWrapper}>
							<Text style={Styles.label}>Descrição</Text>
							<Text style={Styles.maxInputText}>(Max 100 caracteres)</Text>
						</View>
						<InputStyled
							type="textArea"
							multiline
							maxLength={100}
							numberOfLines={4}
							autoCorrect={false}
							onChangeText={setDescription}
						/>
					</View>
					<View style={Styles.buttonWrapper}>
						<RawButton value="Agendar" onPress={handleSave} />
					</View>
				</Background>
			</ScrollView>
			<ModalView
				visible={openGuildModal}
				closeModal={() => setOpenGuildModal(!openGuildModal)}
			>
				<GuildsModal handleSelectGuild={handleSelectGuild} />
			</ModalView>
		</KeyboardAvoidingView>
	);
};

export default AppointmentCreate;
