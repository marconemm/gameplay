import { StyleSheet } from "react-native";
import { getBottomSpace } from "react-native-iphone-x-helper";
import Theme from "../../global/styles/theme";

const { colors, fonts } = Theme;

const Styles = StyleSheet.create({
	bannerWrapper: {
		flex: 1,
		justifyContent: "flex-end",
		paddingHorizontal: 24,
		marginBottom: 20,
	},
	banner: {
		width: "100%",
		height: 234,
		marginBottom: 20,
	},
	title: {
		fontFamily: fonts.title700,
		fontSize: 28,
		color: colors.heading,
	},

	subtitle: {
		fontFamily: fonts.title400,
		fontSize: 14,
		lineHeight: 20,
		color: colors.heading,
	},
	membersList: {
		marginTop: 27,
		marginLeft: 24,
	},
	buttonWrapper: {
		paddingHorizontal: 24,
		paddingVertical: 20,
		marginBottom: getBottomSpace(),
	},
});

export default Styles;
