import React, { useEffect, useState } from "react";
import type { RouteParams, WidgetData } from "../../@Types";
import BannerImage from "../../assets/banner.png";
import {
	View,
	Text,
	ImageBackground,
	FlatList,
	Alert,
	Share,
	Platform,
} from "react-native";
import Styles from "./styles";
import Theme from "../../global/styles/theme";
import Background from "../../components/Background";
import Header from "../../components/Header";
import ListHeader from "../../components/ListHeader";
import ButtonIcon from "../../components/Buttons/IconButton";
import HorizontalRule from "../../components/HorizontalRule";
import Loading from "../../components/Loading";
import Member from "../../components/Member";
import { BorderlessButton } from "react-native-gesture-handler";
import { Fontisto } from "@expo/vector-icons";
import { useRoute } from "@react-navigation/native";
import api from "../../Services/api";
import * as Linking from "expo-linking";

const AppointmentDetails: React.FC = () => {
	const { primaryRed } = Theme.colors;
	const route = useRoute();
	const { item: currentGuild } = route.params as RouteParams;
	const [widgetData, setWidgetData] = useState<WidgetData>();
	const [isLoading, setIsLoading] = useState<boolean>(true);

	const fetchOnLineMembers = async () => {
		try {
			const response = await api.get(
				`/guilds/${currentGuild.guild.id}/widget.json`,
			);

			setWidgetData(response.data);
		} catch {
			Alert.alert('Favor, verificar a liberação para "widget" no Discord.');
		} finally {
			setIsLoading(false);
		}
	};

	const handleLinking = () => {
		if (!!widgetData) {
			Linking.openURL(widgetData.instant_invite);
		}
	};

	useEffect(() => {
		fetchOnLineMembers();
	}, []);

	const handleShareInvite = () => {
		const msg =
			Platform.OS === "ios"
				? `Junte-se a ${currentGuild.guild.name}`
				: widgetData?.instant_invite;

		Share.share({
			message: msg,
			url: widgetData?.instant_invite || "",
		});
	};

	return (
		<Background>
			<Header
				title={"Detalhes"}
				action={
					<BorderlessButton onPress={handleShareInvite}>
						{!!widgetData && (
							<Fontisto name={"share"} size={24} color={primaryRed} />
						)}
					</BorderlessButton>
				}
			/>
			<ImageBackground style={Styles.banner} source={BannerImage}>
				<View style={Styles.bannerWrapper}>
					<Text style={Styles.title}>{currentGuild.guild.name}</Text>
					<Text style={Styles.subtitle}>{currentGuild.description}</Text>
				</View>
			</ImageBackground>

			{isLoading ? (
				<Loading />
			) : (
				<>
					<ListHeader
						title="Participante(s)"
						subtitle={`total ${widgetData?.members.length}`}
					></ListHeader>
					<FlatList
						data={widgetData?.members}
						keyExtractor={item => item.id}
						renderItem={({ item }) => <Member data={item} />}
						ItemSeparatorComponent={HorizontalRule}
						style={Styles.membersList}
					></FlatList>
				</>
			)}
			<View style={Styles.buttonWrapper}>
				<ButtonIcon value="Entrar na partida" onPress={handleLinking} />
			</View>
		</Background>
	);
};

export default AppointmentDetails;
