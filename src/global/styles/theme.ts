const Theme = {
	colors: {
		secondary800: "#0A1033" /**secondary100*/,
		secondary700: "#0D133D",
		secondary600: "#0E1647",
		secondary550: "#171F52",
		secondary500: "#1B2565",
		secondary400: "#1B2565",
		secondary300: "#243189",
		secondary200: "#1D2766",
		secondary100: "#495BCC" /** secondary30,*/,

		overlay: "rgba(0,0,0,0.7)",
		highlight: "#ABB1CC",
		on: "#32BD50",
		heading: "#DDE3F0",

		lineRed: "#991F36",
		primaryRed: "#E51C44",
		primaryYellow: "#FAA81A",

		discord: "#7289DA",
	},
	fonts: {
		text400: "Inter_400Regular",
		text500: "Inter_500Medium",
		title400: "Rajdhani_400Regular",
		title500: "Rajdhani_500Medium",
		title700: "Rajdhani_700Bold",
	},
};

export default Theme;
