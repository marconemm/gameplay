# Gameplay Mobile App (V1.0)
### _Status do Projeto:_ 🚧 **Aceitando Contribuições...** 🚧
___
___

### **Este projeto é o resultado de intensa dedicação nos últimos 6 meses no curso de **_#fullStack_** da [Kenzie Academy Brasil](https://kenzie.com.br/), associados com a gigantesca quantidade de conteúdo sobre React Native presente em sua respectiva trilha na NLW6, promovida pela [RocketSeat](https://rocketseat.com.br/).**

### Apresentação:

- ### Descrição: o **_Gameplay_** é um aplicativo destinado a agendamentos de partidas de jogos on-line entre o usurário e sua rede de contatos.
- Possui autenticação baseada no framework [**_OAuth 2.0_**](https://datatracker.ietf.org/doc/html/rfc6749), através de sua implementação com os [**_servidores de autenticação do Discord_**](https://discord.com/developers/docs/topics/oauth2).
- Nesta _"versão 1.0"_, o **_Gameplay_** ainda está persistindo os dados dos respectivos agendamentos de modo local assim como não está integrado com funcionalidades que demandam a presença de bots como _middleware_.

### Tecnologias aplicadas:

- Linguagem aplicada: [**_TypeScript_**](https://www.typescriptlang.org/) ;
- Versionamento via [**_Git_**](https://git-scm.com/) ;
- Conhecimentos básicos no framework [**_React Native_**](https://reactnative.dev/) ; e
- Autenticação via framework **_OAuth 2.0 Discord_**.

### Demonstração:

Vide vídeo apresentação no seguinte [link](https://youtu.be/Jq3_31pEjsY).

___
___
### Developer:

<a href="https://www.linkedin.com/in/marconemm/">
 <img style="border-radius: 50%;" src="https://avatars.githubusercontent.com/u/15804964?s=400&amp;u=60f45399d863c1410217fc6666bc628c43f554dd&amp;v=4" width="100px;" alt=""/>
 <br />
 <sub><b>Marcone Melo</b></sub></a>

[![GitLab Badge](https://img.shields.io/badge/-Marcone_Melo-black?style=plastic&logo=GitLab&logoColor=yellow&link=https://gitlab.com/marconemm)](https://gitlab.com/marconemm)
[![GitHub Badge](https://img.shields.io/badge/-Marcone_Melo-black?style=plastic&logo=GitHub&logoColor=white&link=https://github.com/marconemm)](https://github.com/marconemm)
[![Linkedin Badge](https://img.shields.io/badge/-Marcone_Melo-blue?style=plastic&logo=Linkedin&logoColor=white&link=https://www.linkedin.com/in/marconemm/)](https://www.linkedin.com/in/marconemm/)
[![Yahoo Badge](https://img.shields.io/badge/-marconemendonca@ymail.com-c14438?style=plastic&logo=Yahoo!&logoColor=white&link=mailto:marconemendonca@ymail.com)](mailto:marconemendonca@ymail.com)
