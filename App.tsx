import React from "react";
import { Inter_400Regular, Inter_500Medium } from "@expo-google-fonts/inter";
import {
	Rajdhani_400Regular,
	Rajdhani_500Medium,
	Rajdhani_700Bold,
} from "@expo-google-fonts/rajdhani";
import AppLoading from "expo-app-loading";
import { useFonts } from "expo-font";
import { StatusBar, LogBox } from "react-native";
import Background from "./src/components/Background";
import Providers from "./src/Providers";
import Routes from "./src/Routes";

LogBox.ignoreLogs(["You are not currently signed in to Expo"]);

const App = () => {
	const [isReady] = useFonts({
		Inter_400Regular,
		Inter_500Medium,
		Rajdhani_400Regular,
		Rajdhani_500Medium,
		Rajdhani_700Bold,
	});

	if (!isReady) {
		return <AppLoading />;
	}

	return (
		<Background>
			<StatusBar
				barStyle="light-content"
				backgroundColor="transparent"
				translucent
			/>
			<Providers>
				<Routes />
			</Providers>
		</Background>
	);
};

export default App;
